from argparse import ArgumentParser

parser = ArgumentParser()
parser.add_argument('--name', type=str, required=True)
parser.add_argument('--do_sample', action='store_true')
parser.add_argument('--top_k', default=50, type=int)
parser.add_argument('--num_beams', default=1, type=int)
parser.add_argument('--early_stopping', action='store_true')
parser.add_argument('--temperature', default=1.0, type=float)
parser.add_argument('--top_p', default=1.0, type=float)

args = vars(parser.parse_args())
name = args.pop("name")


from tqdm.auto import trange
import json
from transformers import MT5Tokenizer, MT5ForConditionalGeneration
from tw_rouge import get_rouge

data = []
ans = []
with open('public.json') as f:
    ss = f.read().split('\n')
    for s in ss:
        if len(s) == 0:
            continue
        o = json.loads(s)
        data.append(o['maintext'])
        ans.append(o['title'])


tokenizer = MT5Tokenizer.from_pretrained('model')
model = MT5ForConditionalGeneration.from_pretrained("model").cuda()

predictions = []

for i in trange(0, len(data), 16):
    input_ids = tokenizer(data[i:i + 16], return_tensors="pt", padding=True, max_length=1024, truncation=True).input_ids.cuda()
    outputs = model.generate(input_ids, max_length=128, **args)
    predictions += tokenizer.batch_decode(outputs, skip_special_tokens=True, clean_up_tokenization_spaces=True)

predictions = list(map(lambda y: "欸" if len(y) == 0 else y, map(lambda x: x.strip(), predictions)))

with open(name, 'w') as f:
    f.write('\n'.join(predictions))

res = get_rouge(predictions, ans)
with open('res-' + name, 'w') as f:
    f.write(json.dumps(res))
print(res)
