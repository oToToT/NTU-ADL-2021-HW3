import json
import sys

# origin input: sys.argv[1]
# origin output: sys.argv[2]
# target output: sys.argv[3]

answer_id = []
with open(sys.argv[1]) as f:
    for s in f.read().split('\n'):
        if s == '':
            continue
        answer_id.append(json.loads(s)['id'])

res = []
with open(sys.argv[2]) as f:
    pos = 0
    for s in f.read().split('\n'):
        res.append(json.dumps({
            "id": answer_id[pos],
            "title": s
        }))
        pos += 1

open(sys.argv[3], "w").write("\n".join(res))
