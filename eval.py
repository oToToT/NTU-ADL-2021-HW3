import json
import argparse
from tw_rouge import get_rouge


def main(args):
    refs, preds = {}, {}

    with open(args.reference) as file:
        for line in file:
            line = json.loads(line)
            refs[line['id']] = line['title']

    with open(args.submission) as file:
        for line in file:
            line = json.loads(line)
            preds[line['id']] = line['title']

    ref_keys = refs.keys()
    pred_keys = preds.keys()
    keys = ref_keys & pred_keys
    for key in pred_keys - keys:
        print(f"unknown key '{key}' in submission")
    for key in ref_keys - keys:
        print(f"key '{key}' not found in submission")
    refs = [refs[key] for key in keys]
    preds = [preds[key] for key in keys]

    print(json.dumps(get_rouge(preds, refs), indent=2))


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-r', '--reference', required=True)
    parser.add_argument('-s', '--submission', required=True)
    args = parser.parse_args()
    main(args)
