# Homework 3 ADL NTU 2021 Spring

## Environment

```bash
pip install -r requirements.txt
pip install -e tw_rouge
```

## Testing

```bash
bash download.sh
bash run.sh INPUT_FILE OUTPUT_FILE
```

## Training

### Download Dataset

```bash
bash download_dataset.sh
```

### Train RL Model

```bash
python3.8 run_summarization_gan.py \
  --model_name_or_path google/mt5-small \
  --do_train \
  --train_file train.json \
  --output_dir $OUTPUT_DIR \
  --per_device_train_batch_size 4 \
  --per_device_eval_batch_size 8 \
  --predict_with_generate \
  --cache_dir ./cache \
  --test_file public.json \
  --do_predict \
  --summary_column title \
  --text_column maintext \
  --num_train_epoch 30 \
  --num_beams 8 \
  --ignore_pad_token_for_loss False
```

### Train Simple Model

```bash
python3.8 run_summarization.py \
  --model_name_or_path google/mt5-small \
  --do_train \
  --train_file train.json \
  --output_dir $OUTPUT_DIR \
  --per_device_train_batch_size 4 \
  --per_device_eval_batch_size 8 \
  --predict_with_generate \
  --cache_dir ./cache \
  --test_file public.json \
  --do_predict \
  --summary_column title \
  --text_column maintext \
  --num_train_epoch 30 \
  --num_beams 8 \
  --ignore_pad_token_for_loss False
```

You would get model output at `$OUTPUT_DIR`. If `$OUTPUT_DIR` is `model`, running `run.sh` could also generate the result you want.
