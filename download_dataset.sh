#!/bin/bash

if [ ! -f "data.zip" ]
then
    wget 'https://adl-models.ototot.tk/hw3/data.zip'
fi

if [ ! -d 'data/' ]
then
    unzip data.zip
fi

if [ ! -f "public.json" ]
then
    ln -s data/public.jsonl public.json
fi
if [ ! -f "train.json" ]
then
    ln -s data/train.jsonl train.json
fi
