#!/bin/bash

python3.8 run_summarization.py --model_name_or_path model \
  '--output_dir' 'model' \
  '--per_device_train_batch_size' '4' \
  '--per_device_eval_batch_size' '4' \
  '--predict_with_generate' \
  '--cache_dir' './cache' \
  '--test_file' $1 \
  '--do_predict' \
  '--summary_column' 'title' \
  '--text_column' 'maintext' \
  '--num_beams' '8' \
  '--ignore_pad_token_for_loss' 'False' \
  '--validation_file' $1 \
  '--do_eval'

python3.8 run_format.py $1 model/generated_predictions.txt $2
